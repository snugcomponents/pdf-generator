<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator\DI;

use Nette\DI\CompilerExtension;
use Nette\DI\Helpers;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

class PdfGeneratorExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'url' => Expect::string()->required(),
            'tempDir' => Expect::string()->required(),
            'username' => Expect::string()->required(),
            'password' => Expect::string()->required(),
        ])->castTo('array');
    }

    public function loadConfiguration()
    {
        $this->compiler->loadDefinitionsFromConfig(
            Helpers::expand(
                $this->loadFromFile(__DIR__ . '/Config/services.neon')['services'],
                (array) $this->config,
            ),
        );
    }
}
