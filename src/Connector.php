<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator;

use Nette\SmartObject;
use Snugcomponents\Utils\Curl;
use Snugcomponents\Utils\Curl\JsonRequest;

abstract class Connector
{
    use SmartObject;

    protected const
        EXTENSION_PDF = '.pdf',
        EXTENSION_JPG = '.jpg';

    public function __construct(
        private Config $config,
    ) { }

    protected function connect(array $data, string $extension): string {
        ignore_user_abort(true);    // when user terminates downloading, then pdf generates and deletes anyway

        $request = JsonRequest::create($this->config->url);
        $request->setUserPWD($this->config->username, $this->config->password);
        $request->setBody($data);
        $request->waitForResponse();

        $curl = Curl::create();
        $response = $curl->exec($request);
        $httpCode = $curl->getInfo(CURLINFO_HTTP_CODE);
        $curl->close();

        if ($response === false || $httpCode !== 200) {
            throw new Exception('Sorry, but request failed.');
        }

        $filename = $this->config->tempDir . '/document_' . rand() . $extension; // $extension is  .pdf or .jpg
        file_put_contents($filename, $response);

        return $filename;
    }
}
