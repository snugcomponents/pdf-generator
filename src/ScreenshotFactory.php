<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator;

class ScreenshotFactory extends Connector
{
    public function create(
        string $htmlOrUrl,
        ?int $screenWidth = null,
        ?int $screenHeight = null,
    ): string {
        return $this->connect([
            'html' => $htmlOrUrl,
            'type' => 'IMG',
            'windowSize' => [
                'width' => $screenWidth,
                'height' => $screenHeight,
            ],
        ], self::EXTENSION_JPG);
    }
}
