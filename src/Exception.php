<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator;

use Exception as BaseException;

class Exception extends BaseException { }
