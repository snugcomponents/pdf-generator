<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator;

class PdfFactory extends Connector
{
    public function create(string $htmlOrUrl): string
    {
        return $this->connect([
            'html' => $htmlOrUrl,
            'type' => 'PDF',
        ], self::EXTENSION_PDF);
    }
}
