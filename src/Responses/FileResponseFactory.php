<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator\Responses;

interface FileResponseFactory
{
    public function create(
        string  $file,
        ?string $name = null,
        ?string $contentType = null,
        bool    $forceDownload = true,
        bool    $deleteFileWhenFinished = false,
    ): FileResponse;
}
