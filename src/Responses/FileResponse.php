<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator\Responses;

use Nette\Application\Response;
use Nette\Application\Responses\FileResponse as NaFileResponse;
use Nette\Http\IRequest;
use Nette\Http\IResponse;
use Nette\SmartObject;

class FileResponse implements Response
{
    use SmartObject;

    private NaFileResponse $fileResponse;

    public function __construct(
        string          $file,
        ?string         $name = null,
        ?string         $contentType = null,
        bool            $forceDownload = true,
        private bool    $deleteFileWhenFinished = false,
    ) {
        $this->fileResponse = new NaFileResponse(
            $file,
            $name,
            $contentType,
            $forceDownload,
        );
    }

    /**
     * Returns the path to a downloaded file.
     */
    public function getFile(): string
    {
        return $this->fileResponse->getFile();
    }

    /**
     * Returns the file name.
     */
    public function getName(): string
    {
        return $this->fileResponse->getName();
    }

    /**
     * Returns the MIME content type of downloaded file.
     */
    public function getContentType(): string
    {
        return $this->fileResponse->getContentType();
    }

    function send(IRequest $httpRequest, IResponse $httpResponse): void
    {
        $this->fileResponse->send($httpRequest, $httpResponse);

        if ($this->deleteFileWhenFinished) {
            unlink($this->fileResponse->getFile());
        }
    }
}
