<?php

declare(strict_types=1);

namespace Snugcomponents\PdfGenerator;

use Nette\SmartObject;

/**
 * @property-read string $url
 * @property-read string $tempDir
 * @property-read string $username
 * @property-read string $password
 */
class Config
{
    use SmartObject;

    public function __construct(
        private string $url,
        private string $tempDir,
        private string $username,
        private string $password,
    ) { }

    public function getUrl(): string { return $this->url; }
    public function getTempDir(): string { return $this->tempDir; }
    public function getUsername(): string { return $this->username; }
    public function getPassword(): string { return $this->password; }
}
