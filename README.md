# PDF Generator

Library for generating PDF files using our API.

## Introduction

This library is Nette extension for making requests to our microservice, which can generate PDF or screenshot from HTML code.
If you want to use this extension, you need to have your own microservice.

## Installation

#### Download
The best way to install `snugcomponents/pdf-generator`, is using Composer:
```sh
$ composer require snugcomponents/pdf-generator
```

#### Registering
Library is Nette ready, so you can register it in your config:
```sh
extensions:
	PdfGenerator: Snugcomponents\PdfGenerator\DI\PdfGeneratorExtension
```

#### Injecting
After registering there are two services, which you may be interested of.
Both of them you can simply inject in your other services:
```php
public function __construct(
	private \Snugcomponents\PdfGenerator\PdfFactory $pdfFactory,
	private \Snugcomponents\PdfGenerator\ScreenshotFactory $screenshotFactory,
) {
    parent::__construct();
    ...
}
```

## Settings
### **All settings are required!!!**
Library **needs** to be set up.
You can do it by inserting following code into your private config:
```sh
PdfGenerator:
	url: 'https://pdfserver.com/' # URL of your microservice for generating PDFs
	tempDir: '%tempDir%'          # temp dir, where will be saved result from microservice
	username: 'username'          # username for Basic authentication
	password: 'password'          # password for Basic authentication
```

## Usage

Usage is pretty simple:

#### PdfFactory:
```php
$pdfFilename = $pdfFactory->create('<p>This is HTML to print</p>');    # send directly HTML which you need to print to PDF
$pdfFilename = $pdfFactory->create('https://htmlurl.com/');            # or send URL address of web page, which you need to be printed into PDF
```
In `$pdfFilename` you will have path to generated pdf. It will be saved in temp dir, which you provided by config.
It is highly recommended to move file to another place.

#### ScreenshotFactory:
```php
$imgFilename = $screenshotFactory->create(
    '<p>This is HTML to print</p>',     # HTML or URL
    1024,                               # width of printing screen - OPTIONAL
    768,                                # height of printing screen - OPTIONAL
);
```
In `$imgFilename` you will have path to generated **jpg** image. It will be saved in temp dir, which you provided by config.
It is highly recommended to move file to another place.

#### Immediate file download

In case you need to download generated file without sawing it, you can do it by another service called `FileResponse`

Little example: (For Presenters only)
```php
#[Inject] public PdfFactory $pdfFactory;
#[Inject] public \Snugcomponents\PdfGenerator\Responses\FileResponseFactory $fileResponseFactory;

public function actionGenerate(): void
{
    $filePath = $this->pdfFactory->create('<p>This is HTML to print</p>');
    $response = $this->fileResponseFactory->create(
        file:                   $filePath,          # path to file to download
        name:                   'document.pdf'      # name for downloaded file      - OPTIONAL
        contentType:            'application/pdf'   # Content-Type of file          - OPTIONAL
        forceDownload:          true,               # force download?               - OPTIONAL (default true)
        deleteFileWhenFinished: true,               # delete file after download?   - OPTIONAL (default false)
    );
    $this->sendResponse($response);
}
```

## Setting up print to PDF

If you need to force your own setting for printing to PDF like remove border, hide images etc., then you can just add classic `@media print` css property to your stylesheet.

## Conclusion
This extension requires PHP8.1, Nette3.1, and it is property of [SnugDesign](https://www.snugdesign.cz/) © 2023
